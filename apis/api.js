import http from "@/apis/http.js"

const api = {
  //---------------------------user-------------------------
  //登录
  login: (data) => {
    return http.post('/login', data)
  },
  //注册
  register: (data) => {
    return http.post('/wx/user/register', data)
  },
  //获取验证码
  getCaptcha: () => {
    return http.get('/wx/user/captcha')
  },
  //根据token查询用户信息
  getUserInfo: (data) => {
    return http.get('/wxInfo?token=' + data)
  },
  //根据用户id查询用户
  getUserInfoById: (id) => {
    return http.get('/sysUser/' + id)
  },
  //修改用户
  updateUser: (data) => {
    return http.put('/sysUser', data)
  },
  //退出登录
  logout: () => {
    return http.post('/logout')
  },
  //首页查询前八条数据
  getEightList: () => {
    return http.post('/subject/subject/noAuth/wx/subject')
  },
  //首页查询列表分页
  getSubjectPage: (data) => {
    return http.post('/subject/subject/noAuth/wx/subject/page', data)
  },
  //查看subject详情
  getSubjectDetails: (id) => {
    return http.get('/subject/subject/wx/subject/detail?id=' + id)
  },
  //查询评论记录
  getSubjectRecords: (id) => {
    return http.get('/subject/subject/wx/subject/record?id=' + id)
  },
  //查询我的点赞状态
  getSubjectMineLike: (id) => {
    return http.get('/subject/subject/wx/subject/mineLike?id=' + id)
  },
  //收藏点赞评论
  commentSubject: (data) => {
    return http.post('/wx/like/comment', data)
  },
  // ---------------------message----------------------
  // 根据类型查询消息列表
  getMessageList: (data) => {
    return http.post('/wx/info/message', data)
  },
  //查询通知列表
  getNoticeList: (data) => {
    return http.post('/wx/notice/message', data)
  },
  // ---------------------appoint----------------------
  //查询可预约时间列表
  getIntervalList: () => {
    return http.get('/interval/wx/list')
  },
  //根据subjectId和选择的日期查询已被预约的时间段
  getMineHistory: (data) => {
    return http.post('/interval/wx/history/list', data)
  },
  //预约
  appointInterval: (data) => {
    return http.post('/interval/wx/appoint', data)
  },
  //查询我的预约列表
  getMineAppoint: (data) => {
    return http.post('/interval/wx/mineDay', data)
  },
  //查询我的预约列表分页
  getMineInterval: (data) => {
    return http.post('/interval/wx/mine', data)
  },
  //取消预约
  cancelInterval: (id) => {
    return http.get('/interval/wx/cancel?id=' + id)
  },
  //完成预约
  completeInterval: (id) => {
    return http.get('/interval/wx/complete?id=' + id)
  },
  // --------------------order--------------
  //新增收货地址
  insertAddress: (data) => {
    return http.post('/address', data)
  },
  //修改收货地址
  updateAddress: (data) => {
    return http.post('/address', data)
  },
  //删除地址
  deleteAddress: (data) => {
    return http.del('/address', data)
  },
  //根据id查询详情
  selectAddressById: (id) => {
    return http.get('/address?id=' + id)
  },
  //查询我的默认收货地址
  getDefaultAddr: () => {
    return http.get('/address')
  },
  //查询所有地址
  getAllAddress: () => {
    return http.post('/address/list')
  },
  //设置默认地址
  setDefaultAddr: (id) => {
    return http.put('/address/' + id)
  },
  //生成订单
  createOrder: (data) => {
    return http.post('/order/create', data)
  },
  //立即支付
  payOrder: (data) => {
    return http.post('/order/pay', data)
  },
  //查询订单列表
  getOrderList: (data) => {
    return http.post('/order/page', data)
  },
  //查询订单红点
  getOrderDot: () => {
    return http.get('/order/dot')
  }

}

export default api