import request from '@/request/request.js'

/**
 * post请求
 * @param url 地址
 * @param data 数据
 * @returns {Promise<any>}
 */
const post = async function(url, data) {
  return await new Promise((resolve, reject) => {
    request({
        url: url,
        method: 'post',
        data: data
      }).then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * get请求
 * @param url
 * @returns {Promise<any>}
 */
const get = async function(url) {
  return await new Promise((resolve, reject) => {
    request({
      url: url,
      method: 'get'
    }).then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * put请求
 * @param url
 * @param params
 * @returns {Promise<any>}
 */
const put = async function(url, params) {
  return await new Promise((resolve, reject) => {
    request({
      url: url,
      method: 'put',
      data: params
    }).then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * delete请求
 * @param url
 * @param params
 * @returns {Promise<any>}
 * @constructor
 */
const del = async function(url, params) {
  return await new Promise((resolve, reject) => {
    request({
      url: url,
      method: 'delete',
      data: params
    }).then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}
/**
 * 根据id删除
 * @param url
 * @returns {Promise<unknown>}
 */
const deleteId = async function(url) {
  return await new Promise((resolve, reject) => {
    request({
      url: url,
      method: 'delete'
    }).then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

export default {
  post,
  get,
  put,
  del,
  deleteId
}