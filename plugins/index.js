import tab from './tab'
import auth from './auth'
import modal from './modal'
import option from './option'
import api from '../apis/api'

export default {
  install(Vue) {
    // 接口调用
    Vue.prototype.$api = api
    // 页签操作
    Vue.prototype.$tab = tab
    // 认证对象
    Vue.prototype.$auth = auth
    // 模态框对象
    Vue.prototype.$modal = modal
    // 枚举
    Vue.prototype.$option = option
  }
}